﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FleetManager : MonoBehaviour
{
    #region SetGameManagers
    GameManagers gameManagers;
    public GameManagers GameManagers
    {
        get
        {
            return gameManagers;
        }
        set
        {
            if (!gameManagers)
            {
                gameManagers = value;
            }
        }
    }
    #endregion

    public delegate void StopShootDel();
    public static event StopShootDel StopShootEvent;

    public Fleet _fleet;
    [SerializeField] List<Fleet> fleetList;

    private void Awake()
    {
        HPComponent.ShipKillEvent += ShipKill;
    }

    public void CreateFleet(EnumSide side, int idFleetSlotMask, int idShipMask)
    {
        Fleet fleet = Instantiate(_fleet);
        fleetList.Add(fleet);
        
        fleet.Side = side;
        fleet.SetSlotMask(idFleetSlotMask);

        gameManagers.PutShipInFleet(fleet, idShipMask);       
    }
    public void RemoveFleet(Fleet fleet)
    {
        if(fleetList.Contains(fleet))
        {
            if(StopShootEvent != null)
            {
                StopShootEvent();
            }

            fleetList.Remove(fleet);            
            Debug.Log("Remove fleet:" + fleet);
            Destroy(fleet.gameObject);
        }
    }

    public void ShipKill(Ship ship)
    {
        foreach(Fleet fleet in fleetList)
        {
            fleet.ShipKill(ship);
        }        
    }
}
