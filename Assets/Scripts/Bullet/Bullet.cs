﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public EnumSide Side;

    public float speed = 3.5f;  
    float liveTime;
    public int damage;
    public Ship ship;

    private void Awake()
    {
        Side = GetComponent<SideComponent>().Side;
    }

    void Update()
    {
        FlyAway();
        CheckDistanceToDestroy();
        CheckSpeedToDestroy();
    }
    void FlyAway()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);        
    }
    void CheckDistanceToDestroy()
    {
        if(transform.position.y > 6.5f)
        {
            Destroy(gameObject);
        }
    }
    void CheckSpeedToDestroy()
    {
        if (speed == 0)
        {
            liveTime += Time.deltaTime;
            if (liveTime > 1)
            {
                Destroy(gameObject);
            }
        }
    }
    public void SetOwner(Ship setShip)
    {
        ship = setShip;
        SetDamage();
        Rotate();
    }
    void SetDamage()
    {
        if(ship.name == "Shturmovik(Clone)")
        {
            damage = 1;
        }
        else if(ship.name == "LazerGun(Clone)")
        {
            damage = 2;
        }
        else if (ship.name == "Flagman(Clone)")
        {
            damage = 5;
        }
        else if (ship.name == "BigSpider(Clone)")
        {
            damage = 2;
        }
        else if (ship.name == "MissileShip(Clone)")
        {
            damage = 3;
        }
        else if (ship.name == "SmallSpider(Clone)")
        {
            damage = 1;
        }
        else
        {
            damage = 1;
        }
    }
    void Rotate()
    {
        if(ship.Side == EnumSide.npc)
        {
            transform.Rotate(0,0,180);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ship")
        {
            Destroy(gameObject);
        }
    }
}
