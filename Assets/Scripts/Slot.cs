﻿using UnityEngine;
using System.Collections;

public class Slot : MonoBehaviour
{
    public EnumSize Size;

    private void Awake()
    {
        Size = GetComponent<SizeComponent>().Size;
    }
    public void SetSize(EnumSize setSize)
    {
        Size = setSize;
        SetName(Size);
    }

    public int row;
    public int column;
    public void SetPos(int setRow, int setColumn)
    {
        row = setRow;
        column = setColumn;

        transform.localPosition = new Vector3(setColumn, setRow, 0);
    }

    public Ship ship;
    public void AddShip(Ship setShip)
    {        
        if(Size < setShip.Size)
        {
            Debug.Log("Slot to small", gameObject);
        }
        else
        {                
            ship = setShip;
            ship.transform.SetParent(transform);
            ship.moveEnable = true;                
        }
    }    

    void SetName(EnumSize size)
    {
        name = "Slos S:" + size;
    }
  
}
