﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScreenPanel : GamePanel
{
    [SerializeField] Button startGameBtn;

    public delegate void StartGameBtnClkDel();
    public static event StartGameBtnClkDel StartGameBtnClkEvent;

    private void Start()
    {
        startGameBtn.onClick.AddListener(StartGameBtnClk);
    }

    void StartGameBtnClk()
    {
        if(StartGameBtnClkEvent != null)
        {
            StartGameBtnClkEvent();
        }
    }
}
