﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    [SerializeField] bool move = true;
    [SerializeField] float planetMoveSpeed = 1.5f;

    void Start()
    {        
        GenerateName();
        SetPosition();
    }
    private void Update()
    {
        Move();
        SetMove();
        StopPlanetMove();        
    }
    void GenerateName()
    {
        name = "P:" + Random.Range(100, 999) + ":" + Random.Range(100, 999) + ".";
    }
    void SetPosition()
    {
        transform.position = new Vector3(0, 7f, 0);
    }
    #region MOVE
    void SetMove()
    {
        move = true;        
    }
    void Move()
    {
        if (move == true)
        {
            transform.Translate(Vector3.down * Time.deltaTime * planetMoveSpeed);
        }
    }
    void StopPlanetMove()
    {
        if (transform.position.y < 1.5f)
        {
            move = false;
        }
    }
    #endregion
}
