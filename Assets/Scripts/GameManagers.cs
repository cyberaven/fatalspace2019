﻿using UnityEngine;
using System.Collections;

public class GameManagers : MonoBehaviour
{
    [SerializeField] GameEventsManager gameEventsManager;    
    [SerializeField] UIManager uIManager;   
    [SerializeField] PlanetManager planetManager;    
    [SerializeField] FleetManager fleetManager;   
    [SerializeField] ShipManager shipManager;   
    [SerializeField] LevelManager levelManager;

    private void Start()
    {
        StartScreenPanel.StartGameBtnClkEvent += StartGameBtnClk;
    }

    public void InitAllManagers()
    {
        gameEventsManager = Instantiate(gameEventsManager, transform);
        uIManager = Instantiate(uIManager, transform);
        planetManager = Instantiate(planetManager, transform);
        fleetManager = Instantiate(fleetManager, transform);
        shipManager = Instantiate(shipManager, transform);
        levelManager = Instantiate(levelManager, transform);

        gameEventsManager.GameManagers = this;
        uIManager.GameManagers = this;
        shipManager.GameManagers = this;
        fleetManager.GameManagers = this;
        levelManager.GameManagers = this;
        planetManager.GameManagers = this;
    }   
    public void StartApp()
    {
        uIManager.StartApp();
    }
    public void CreateFleet(EnumSide side, int idFleetSlotMask, int idShipMask)
    {
        fleetManager.CreateFleet(side, idFleetSlotMask, idShipMask);
    }
    public void RemoveFleet(Fleet fleet)
    {
        fleetManager.RemoveFleet(fleet);
    }
    public void CreatePlanet(int planetType)
    {
        Planet planet = planetManager.CreatePlanet(planetType);
        levelManager.PutPlanetInLevel(planet);
    }
    public void CreateLevel(int lvlNumb)
    {
        levelManager.CreateLevel(lvlNumb);
    }
    public void PutShipInFleet(Fleet fleet, int idShipMask)
    {
        shipManager.PutShipInFleet(fleet, idShipMask);
    }   
    public void LevelEventTrigerStop()
    {        
        levelManager.MoveDisable();
    }
    public void LevelEventTrigerStart()
    {
        levelManager.MoveEnable();        
    }

    public void StartGameBtnClk()
    {
        uIManager.HideAll();

        CreateFleet(EnumSide.plr, 0, 0);//создаем флот игрока.
        CreateLevel(0);
    }

}
