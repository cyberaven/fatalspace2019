﻿using UnityEngine;
using System.Collections;

public class HPComponent : MonoBehaviour, IHaveHP
{
    public delegate void ShipKillDel(Ship ship);
    public static event ShipKillDel ShipKillEvent;

    int _hp;
    public int HP
    {
        get
        {
            return _hp;
        }
        set
        {
            _hp += value;
            if(_hp <= 0)
            {
                Die();
            }
        }
    }

    public void Die()
    {
        if (ShipKillEvent != null)
        {
            ShipKillEvent(this.gameObject.GetComponent<Ship>());
        }
        Destroy(this.gameObject);
    }
}
