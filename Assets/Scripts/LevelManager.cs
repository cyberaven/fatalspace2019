﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
    #region SetGameManagers
    GameManagers gameManagers;
    public GameManagers GameManagers
    {
        get
        {
            return gameManagers;
        }
        set
        {
            if (!gameManagers)
            {
                gameManagers = value;
            }
        }
    }
    #endregion

    [SerializeField] Level _level;
    [SerializeField] Level currentLevel;

    public void CreateLevel(int levelNumb)
    {
        Level level = Instantiate(_level);        
        level.LoadLevel(levelNumb);
        currentLevel = level;
    }
    public void PutPlanetInLevel(Planet planet)
    {
        currentLevel.PutPlanetInLevel(planet);
    }

    public void MoveEnable()
    {
        currentLevel.MoveEnable();
    }
    public void MoveDisable()
    {
        currentLevel.MoveDisable();
    }
}
