﻿internal interface IHaveSide
{
    EnumSide Side { get; set; }
}