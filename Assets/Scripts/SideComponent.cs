﻿using UnityEngine;
using System.Collections;

public class SideComponent : MonoBehaviour, IHaveSide
{
    EnumSide _side;
    public EnumSide Side { get => _side; set => _side = value; }
}
