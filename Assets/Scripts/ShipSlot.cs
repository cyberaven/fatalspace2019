﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipSlot : MonoBehaviour
{
    public EnumSize Size;
    //[SerializeField] int size;
    [SerializeField] Ship ship;

    private void Awake()
    {
        Size = GetComponent<SizeComponent>().Size;
    }


    public void SetSize(EnumSize setSize)
    {        
        Size = setSize;
        name = "Slot: S" + Size;
        //transform.localScale = new Vector3(size, size,0);
    }
    public void SetPosition(int x, int y, int z)
    {
        transform.localPosition = new Vector3(x, y, z);
    }
    public void AddShip(Ship setShip)
    {
        if (setShip.Size <= Size)
        {
            ship = setShip;
            ship.transform.SetParent(transform);
            ship.transform.localPosition = Vector3.zero;
        }
        else
        {
            Debug.Log("<color=red>Ship size:"+setShip.Size+" Slot size:"+Size+"</color>");
        }
    }
}
