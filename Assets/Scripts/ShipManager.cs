﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour
{
    #region SetGameManagers
    GameManagers gameManagers;
    public GameManagers GameManagers
    {
        get
        {
            return gameManagers;
        }
        set
        {
            if (!gameManagers)
            {
                gameManagers = value;
            }
        }
    }
    #endregion

    #region shipMask/shipMaskList
    List<List<int>> shipMask = new List<List<int>>();
    List<List<List<int>>> shipMaskList = new List<List<List<int>>>
    {
        new List<List<int>>//id ship смотреть в инспекторе
                        {
                                new List<int> {1,2,0,2,1},
                                new List<int> {3,1,2,1,3},
                                new List<int> {3,3,3,3,3}
                        },
        new List<List<int>>//id ship смотреть в инспекторе
                        {
                                new List<int> {3,3,3,3,3},
                                new List<int> {3,3,3,3,3},
                                new List<int> {3,3,3,3,3}
                        },
        new List<List<int>>//id ship смотреть в инспекторе
                        {
                                new List<int> {4,5,5,5,4},
                                new List<int> {4,4,4,4,4},
                                new List<int> {4,4,4,4,4}
                        },
        new List<List<int>>//id ship смотреть в инспекторе
                        {
                                new List<int> {1,1,1,1,1},
                                new List<int> {2,2,2,2,2},
                                new List<int> {1,3,1,3,1}
                        }
        };
    #endregion

    private void Awake()
    {
        HPComponent.ShipKillEvent += ShipKill;
    }

    public List<Ship> shipList;
    public void PutShipInFleet(Fleet fleet, int idShipMask)
    {
        List<List<int>> fleetSlotMask = fleet.GetSlotMask();

        for (int i = 0; i < fleetSlotMask.Count; i++)
        {
            for (int a = 0; a < fleetSlotMask[i].Count; a++)
            {
                if (shipMaskList[idShipMask] != null)
                {
                    shipMask = shipMaskList[idShipMask];

                    Ship ship = Instantiate(shipList[shipMask[i][a]]);
                    ship.type = shipMask[i][a];                   
                    ship.Side = fleet.Side;

                    Slot slot = fleet.GetSlot(i, a);
                    fleet.PutShipInSlot(ship, slot);
                }
                else
                {
                    Debug.Log("No idShipMask:"+ idShipMask + " in shipMaskList", gameObject);
                }
            }
        }
    }

    public void ShipKill(Ship ship)
    {        
        shipList.Remove(ship);
    }
}
