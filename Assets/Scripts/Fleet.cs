﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fleet : MonoBehaviour
{
    #region SetGameManagers
    GameManagers gameManagers;
    public GameManagers GameManagers
    {
        get
        {
            return gameManagers;
        }
        set
        {
            if (!gameManagers)
            {
                gameManagers = value;
            }
        }
    }
    #endregion

    [SerializeField] bool move = false;
    [SerializeField] float fleetMoveSpeed = 1.5f;


    public EnumSide Side;
    
    //#region IsPlrFleet
    //public bool plrFleet = false;
    //public void IsPlrFleet(bool setPlrFleet)
    //{
    //    plrFleet = setPlrFleet;
    //    SetMove();
    //}
    //#endregion

    #region Slot
    [SerializeField] List<List<int>> slotMask;
    [SerializeField] List<List<List<int>>> slotMaskList = new List<List<List<int>>>
    {
       new List<List<int>>
            {//карта флота, матрица слотов int = slotSize
                    new List<int> {2,2,3,2,2},//moveable = false,size = 2 ,size = 2,size = 3 ...
                    new List<int> {1,2,2,2,1},
                    new List<int> {1,1,1,1,1}
            },//moveable = true,size = 2 ,size = 2,size = 3 ...
       new List<List<int>>
            {//карта флота, матрица слотов int = slotSize
                    new List<int> {1,1,1,1,1},//moveable = false,size = 2 ,size = 2,size = 3 ...
                    new List<int> {1,1,1,1,1},
                    new List<int> {1,1,1,1,1}
            },
       new List<List<int>>
            {//карта флота, матрица слотов int = slotSize
                    new List<int> {3,3,3,3,3},//moveable = false,size = 2 ,size = 2,size = 3 ... 
                    new List<int> {3,3,3,3,3},
                    new List<int> {3,3,3,3,3}
            },
       new List<List<int>>
            {//карта флота, матрица слотов int = slotSize
                    new List<int> {1,1,3,1,1}//moveable = false,size = 2 ,size = 2,size = 3 ... 
            }
    };
    public void SetSlotMask(int idFleetSlotMask)
    {
        if (slotMaskList[idFleetSlotMask] != null)
        {
            slotMask = slotMaskList[idFleetSlotMask];
            CreateSlot();
        }
        else
        {
            Debug.Log("No SlotMas ID:" + idFleetSlotMask, gameObject);
        }
    }
    public List<List<int>> GetSlotMask()
    {
        return slotMask;
    }

    public Slot _slot;    
    void CreateSlot()
    {
        for (int i = 0; i < slotMask.Count; i++)
        {
            for (int a = 0; a < slotMask[i].Count; a++)
            {                
                Slot slot = Instantiate(_slot, transform);
                slotList.Add(slot);
                slot.SetSize((EnumSize)slotMask[i][a]);
                slot.SetPos(i, a);                
            }
        }
    }

    [SerializeField] List<Slot> slotList;   
    public Slot GetSlot(int row, int colum)
    {        
        for (int i = 0; i < slotList.Count; i++)
        {
            Slot slot = slotList[i];

            if(slot.row == row && slot.column == colum)
            {
                return slot;
            }
        }
        Debug.Log("No slot in row:" + row + " colum:" + colum);
        return null;
    }
    #endregion

    [SerializeField] List<Ship> shipInFleetList = new List<Ship>();

    Ship shipSelect1;
    Ship shipSelect2;

    public void PutShipInSlot(Ship ship, Slot slot)
    {
        slot.AddShip(ship);
        shipInFleetList.Add(ship);        
    }

    private void Awake()
    {
        Side = GetComponent<SideComponent>().Side;
    }

    private void Start()
    {        

        Ship.ShipSelectToMoveEvent += ShipSelectToMove;

        SetPosition();        
    }
    private void Update()
    {
        Move();
        StopFleetMove();
        CheckPosToDestroy();
        CheckShipInFleet();
    }

    #region MOVE
    void SetMove()
    {
        if(Side == EnumSide.npc)
        {
            move = true;
        }
    }
    void Move()
    {
        if (move == true)
        {
            transform.Translate(Vector3.up * Time.deltaTime * fleetMoveSpeed);
        }
    }    
    void StopFleetMove()
    {
        if (transform.position.y < 4.5f)
        {            
            move = false;
        }
    }
    #endregion

    void SetPosition()
    {
        if (Side == EnumSide.plr)
        {
            transform.position = new Vector3(-2, -3.5f, 0);
        }
        else
        {
            transform.Rotate(0, 0, 180);
            transform.position = new Vector3(2, 10f, 0);
        }
    }
    void CheckPosToDestroy()
    {
        if (transform.position.y < -5)
        {
            gameManagers.RemoveFleet(this);
        }
    }

    public void ShipKill(Ship setShip)
    {
        foreach(Ship ship in shipInFleetList)
        {
            if(ship == setShip)
            {
                shipInFleetList.Remove(setShip);
            }
        }        
    }
    void CheckShipInFleet()
    {
        if(shipInFleetList.Count == 0)
        {
            gameManagers.RemoveFleet(this);
        }        
    }

   void ShipSelectToMove(Ship ship)
    {
        if(shipSelect1 == null)
        {
            shipSelect1 = ship;
            Debug.Log("shipSelect1");
        }
        else
        {
            shipSelect2 = ship;
            Debug.Log("shipSelect2");
        }

        if(shipSelect1 != null && shipSelect2 != null)
        {
            SwapShip(shipSelect1, shipSelect2);
            shipSelect1 = null;
            shipSelect2 = null;
        }
    }
    void SwapShip(Ship shipSelect1, Ship shipSelect2)
    {
        Debug.Log("Swap ships"); 

        Slot slot1 = null;
        Slot slot2 = null;

        foreach (Slot slot in slotList)
        {
            if(slot.ship == shipSelect1)
            {
                slot1 = slot;
            }
            if (slot.ship == shipSelect2)
            {
                slot2 = slot;
            }
        }
        
        slot1.AddShip(shipSelect2);
        shipSelect1.moveEnable = true;
                
        slot2.AddShip(shipSelect1);
        shipSelect2.moveEnable = true;
    }



}
