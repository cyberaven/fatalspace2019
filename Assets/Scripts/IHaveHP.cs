﻿public interface IHaveHP
{
    int HP { get; set; }
    void Die();
}
