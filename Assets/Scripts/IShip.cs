﻿using UnityEngine;
internal interface IShip
{
    void Init(EnumSize size, EnumSide side, int hp, float shootTime, Sprite sprite);
}