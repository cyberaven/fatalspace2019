﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlotsLine : MonoBehaviour
{
    public ShipSlot _shipSlot;
    public List<ShipSlot> shipSlotList;

    [SerializeField]bool moveable = false;
    bool reverseMove = false;
    float timeForMove;
    Vector3 tp;

    private void Start()
    {
        tp = transform.position;
    }

    private void Update()
    {
        Move();
    }

    void Move()
    {
        if(moveable == true)
        {
            
            if (reverseMove == false)
            {
                transform.position = Vector3.Lerp(new Vector3(-1, tp.y, tp.z), new Vector3(1, tp.y, tp.z), timeForMove);
            }
            else
            {
                transform.position = Vector3.Lerp(new Vector3(1, tp.y, tp.z), new Vector3(-1, tp.y, tp.z), timeForMove);
            }
            timeForMove += Time.deltaTime;
            if (timeForMove > 1)
            {
                timeForMove = 0;

                if (reverseMove == true)
                {
                    reverseMove = false;
                }
                else
                {
                    reverseMove = true;
                }
            }

        }
        else
        {
            transform.position = new Vector3(0, tp.y, tp.z);
        }
    }

    public SlotsLine CreateSlot(List<int> slotLineMap)
    {
        if(slotLineMap[0] == 1)
        {
            moveable = true;            
        }
        else
        {
            moveable = false;            
        }

        slotLineMap.RemoveAt(0);

        for (int i = 0; i < slotLineMap.Count; i++)
        {
            ShipSlot shipSlot = Instantiate(_shipSlot, transform);            
            shipSlotList.Add(shipSlot);

            //shipSlot.SetSize(slotLineMap[i]);           
            shipSlot.SetSize((EnumSize)slotLineMap[i]);
            shipSlot.SetPosition(i-(slotLineMap.Count / 2), 0, 0);
        }
        return this;
    }
    public void SetName(string setName)
    {
        name = setName;
    }
    public void SetPosition(int x, int y, int z)
    {
        transform.localPosition = new Vector3(x,y,z);
    }
    
}
