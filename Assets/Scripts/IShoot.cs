﻿public interface IShoot
{
    bool ShootEnable { get; set; }
    float RealTime { get; set; }
    float ShootTime { get; set; }
    void Shoot(Bullet bullet);
}
