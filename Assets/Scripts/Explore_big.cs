﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explore_big : MonoBehaviour
{
    float lifeTime = 1f;
    float time;

    void Update()
    {
        time += Time.deltaTime;

        if(time > lifeTime)
        {
            Destroy(gameObject);
        }
    }
}
