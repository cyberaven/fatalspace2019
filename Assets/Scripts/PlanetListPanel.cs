﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetListPanel : MonoBehaviour
{

    [SerializeField]
    private GameData gameData;

    public GameObject _planetInfo;    

    // Start is called before the first frame update
    void Start()
    {
        CreatePlanetList();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreatePlanetList()
    {
        for (int i = 0; i < gameData.PlanetList.Count; i++)
        {
            GameObject planetInfo = Instantiate(_planetInfo, transform);
            planetInfo.GetComponent<PlanetInfo>().SetPlanetName(gameData.PlanetList[i].name);            
        }
    }
}
