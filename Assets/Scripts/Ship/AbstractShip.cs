﻿using UnityEngine;
using System.Collections;

public abstract class AbstractShip : MonoBehaviour, IShip, IhaveSize, IHaveSide, IHaveHP, IShoot
{
    #region IShip
    public void Init(EnumSize size, EnumSide side, int hp, float shootTime, Sprite sprite)
    {
        Size = size;
        Side = side;
        HP = hp;
        ShootTime = shootTime;
        GetComponent<SpriteRenderer>().sprite = sprite;        
    }
    #endregion

    #region IHaveSize
    EnumSize _size;
    public EnumSize Size { get => _size; set => _size = value; }
    #endregion

    #region IHaveSide
    EnumSide _side;
    public EnumSide Side { get => _side; set => _side = value; }
    #endregion

    #region IHaveHP
    int _hp;
    public int HP
    {
        get
        {
            return _hp;
        }
        set
        {
            _hp += value;
            if(_hp <= 0)
            {
                Die();
            }
        }
    }
    public void Die()
    {
        Destroy(this.gameObject);
    }
    #endregion

    #region IShoot
    [SerializeField] Bullet _bullet;

    bool _shootEnable = true;
    public bool ShootEnable { get => _shootEnable; set => _shootEnable = value; }

    float _realTime = 0f;
    public float RealTime { get => _realTime; set => _realTime = value; }

    float _shootTime;
    public float ShootTime { get => _shootTime; set => _shootTime = value; }
    public void Shoot(Bullet _bullet)
    {
        if (ShootEnable == true)
        {
            RealTime += Time.deltaTime;
            if (RealTime >= ShootTime)
            {
                Bullet bullet = Instantiate(_bullet);
                RealTime = 0f;
            }
        }
    }
    #endregion

    #region UnityMethod
    private void Awake()
    {
        GameEventsManager.ShipShootStartEvent += ShootStart;
        GameEventsManager.ShipShootStopEvent += ShootEnd;
    }
    private void Start()
    {        
    }

    void Update()
    {
        Shoot(_bullet);
      //  MoveToSlot();
    }
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.tag == "Bullet")
    //    {
    //        Bullet bullet = collision.gameObject.GetComponent<Bullet>();

    //        if (bullet.ship.plr != plr)
    //        {
    //            Hit(bullet);
    //            if (bullet.name != "Lazer_Ray(Clone)")
    //            {
    //                Destroy(bullet.gameObject);
    //            }
    //        }
    //    }
    //}
    //private void OnMouseDown()
    //{
    //    if (ShipSelectToMoveEvent != null)
    //    {
    //        ShipSelectToMoveEvent(this);
    //    }
    //}
    #endregion


    #region TODO
    void ShootStart()
    {
        ShootEnable = true;
    }
    void ShootEnd()
    {
        ShootEnable = false;
    }

    #endregion
}
