﻿using UnityEngine;

public abstract class AbstractShoot : MonoBehaviour, IShoot
{
    [SerializeField] Bullet _bullet;
    EnumSide Side;

    [SerializeField] bool _shootEnable = false;
    public bool ShootEnable { get => _shootEnable; set => _shootEnable = value; }

    [SerializeField] float _realTime = 0f;
    public float RealTime { get => _realTime; set => _realTime = value; }

    [SerializeField] float _shootTime = 1f;
    public float ShootTime { get => _shootTime; set => _shootTime = value; }
    public void Shoot(Bullet _bullet)
    {
        if (ShootEnable == true)
        {
            RealTime += Time.deltaTime;
            if (RealTime >= ShootTime)
            {
                Bullet bullet = Instantiate(_bullet);
                bullet.Side = Side;
                bullet.transform.position = transform.position;
                RealTime = 0f;
            }
        }
    }

    void ShootStart()
    {
        ShootEnable = true;
    }
    void ShootEnd()
    {
        ShootEnable = false;
    }

    private void Awake()
    {
        GameEventsManager.ShipShootStartEvent += ShootStart;
        GameEventsManager.ShipShootStopEvent += ShootEnd;
    }

    private void Start()
    {
        Side = GetComponent<SideComponent>().Side;
    }

    private void Update()
    {
        Shoot(_bullet);
    }
}
