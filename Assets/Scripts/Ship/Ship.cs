﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{

    public EnumSide Side;
    public EnumSize Size;
    public int HP;
    
    public int type;  

    public GameObject exposion;

    float moveSpeed = 1f;
    public bool moveEnable = false; 

    public delegate void ShipSelectToMoveDel(Ship ship);
    public static event ShipSelectToMoveDel ShipSelectToMoveEvent;

    private void Awake()
    {
        Side = GetComponent<SideComponent>().Side;
        Size = GetComponent<SizeComponent>().Size;
        HP = GetComponent<HPComponent>().HP;
    }

    void Update()
    {
       MoveToSlot();
    }

    void Hit(Bullet bullet)
    {
        HP -= bullet.damage;       
    }
    void Kill()
    {
        ExploseAnim();       

        //if(ShipKillEvent != null)
        //{
        //    ShipKillEvent(this, fleet);
        //}

        Destroy(gameObject);
    }
    void ExploseAnim()
    {
        GameObject ex = Instantiate(exposion);
        ex.transform.position = transform.position;
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet")
        {
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();

            if (bullet.Side != Side)
            {
                Hit(bullet);
                if (bullet.name != "Lazer_Ray(Clone)")
                {
                    Destroy(bullet.gameObject);
                }
            }
        }       
    }
    private void OnMouseDown()
    {
        if (ShipSelectToMoveEvent != null)
        {
            ShipSelectToMoveEvent(this);
        }
    }

    public void MoveToSlot()
    {
        if (moveEnable == true)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, moveSpeed * Time.deltaTime);

            if (transform.localPosition == Vector3.zero)
            {
                moveEnable = false;
            }
        }
    }
}
