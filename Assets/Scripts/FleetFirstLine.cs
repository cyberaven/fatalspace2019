﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleetFirstLine : MonoBehaviour
{
    //--------FleetFirstLineMove Start
    Vector3 rightDestinationPoint;
    Vector3 startPoint;
    float time;
    bool reverseMode = false;
    public bool moveEnable = true;
    //--------FleetFirstLineMove END

    [SerializeField]
    private GameData gameData;
    // Start is called before the first frame update
    void Start()
    {
        rightDestinationPoint = new Vector3(1.5f, transform.position.y, transform.position.z);       
        startPoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        MoveFrstLine();
    }

    void MoveFrstLine()
    {
        if (gameData.OnPlanet == false)        
        {
            if (reverseMode == false)
            {
                transform.position = Vector3.Lerp(startPoint, rightDestinationPoint, time);                               
            }
            else
            {
                transform.position = Vector3.Lerp(rightDestinationPoint, startPoint, time);
            }

            time += Time.deltaTime;

            if (time > 1)
            {                
                time = 0;

                if(reverseMode == true)
                {
                    reverseMode = false;                    
                }
                else
                {
                    reverseMode = true;                    
                }
            }
        }
        else
        {
            transform.position = new Vector3(0f, transform.position.y, transform.position.z);
        }

    }
}
